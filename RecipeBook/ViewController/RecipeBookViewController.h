//
//  RecipeBookViewController.h
//  RecipeBook
//
//  Created by Rifat Firdaus on 6/19/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeBookViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) IBOutlet UITableView *tableView;

@end
