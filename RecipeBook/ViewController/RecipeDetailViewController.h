//
//  RecipeDetailViewController.h
//  RecipeBook
//
//  Created by Rifat Firdaus on 6/19/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"

@interface RecipeDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *recipePhoto;
@property (weak, nonatomic) IBOutlet UILabel *prepTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ingredientTextView;

@property (nonatomic,strong) Recipe *recipe;

@property (nonatomic, strong)NSString *recipeName;

@end
