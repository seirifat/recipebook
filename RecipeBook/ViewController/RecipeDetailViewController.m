//
//  RecipeDetailViewController.m
//  RecipeBook
//
//  Created by Rifat Firdaus on 6/19/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import "RecipeDetailViewController.h"

@interface RecipeDetailViewController ()

@end

@implementation RecipeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _recipeName;
//    self.title = _recipe.name;
//    self.prepTimeLabel.text = _recipe.prepTime;
//    self.recipePhoto.image = [UIImage imageNamed:_recipe.imageFile];
//    
//    NSMutableString *ingredientText = [NSMutableString string];
//    for (NSString* ingredient in _recipe.ingredients) {
//        [ingredientText appendFormat:@"%@\n",ingredient];
//    }
//    self.ingredientTextView.text = ingredientText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
